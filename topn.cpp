#include <iostream>
#include <set>
#include <stdexcept>
#include <string>

using namespace std;

void usage() {
  cerr << 
  "usage: topn N\n"
  "\tN: postive integer\n";
  exit(2);
}

void process(int n) {
  // This solution assumes that set<int> of n numbers fits in RAM
  // So space complexity is O(n log n)
  // Time complexity for the worst case (when the file is sorted) is O(m log n) where m is number of integers in the file
  set<int> max_set;
  // I assume that the file contains numbers that fit in int type
  int x;
  while (cin >> x) {
    if (max_set.size() < n) {
      max_set.insert(x);
    } else if (*max_set.begin() < x) {
      max_set.erase(max_set.begin());
      max_set.insert(x);
    }
  }
  for (auto it = max_set.rbegin(); it != max_set.rend(); ++it) {
    cout << *it << endl;
  }
}

int main(int argc, char* argv[]) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  if (argc != 2) {
    usage();
  }
  int n;
  try {
    n = stoi(argv[1]);
  } catch (const invalid_argument& ia) {
    usage();
  }
  if (n < 1) {
    usage();
  }
  process(n);
  return 0;
}
